# Assume this file is in $HOME/tildeclub.

HTML_DIR='public_html'
GOPHER_DIR='public_gopher'

CURR_DIR=$(pwd)

# TODO: check if CURR_DIR is $HOME/tildeclub

# Go into $HOME, delete the public_{html,gopher} directories and move back
# here.
cd ..
rm -rf $HTML_DIR $GOPHER_DIR
cd $CURR_DIR

# Move the new public_{html,gopher} directories into place.
mv $HTML_DIR $GOPHER_DIR $HOME

# And, finally, delete the cloned repo
cd ..
rm -rf $CURR_DIR
